<?php

namespace Drupal\external_register_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for external_register_redirect.
 */
class ExternalRegisterRedirectSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'external_register_redirect.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'external_register_redirect_settings_form';
  }

  /**
   * Build the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('external_register_redirect.settings');
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $markup =
      '<h3>' . $this->t('How does External Register Redirect work') . '</h3>' .
      '<div>' .
      $this->t('If a user gets to "@host/user/register?@param=https://redirect_url" 
      and "redirect_url" is in the list of allowed domains, then the user will be 
      redirected there after the first time they edit their profile (ie: after 
      one time login).', ['@host' => $host, '@param' => $config->get('param')]) .
      '</div><br/><hr/>';

    $form['info'] = [
      '#markup' => $markup,
    ];

    $form['param'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parameter name'),
      '#description' => $this->t('Name of the GET parameter to retrieve.'),
      '#default_value' => $config->get('param') ?? 'redirect',
    ];

    $form['allowed_domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed domains'),
      '#description' => $this->t('
        Separate domains by line breaks and do NOT include the protocol. 
        So `domain.com` instead of `https://domain.com`. 
        Add all domain variations as it will not check for subdomains, 
        so add `www.domain.com` and `domain.com` if needed.
      '),
      '#default_value' => $config->get('allowed_domains'),
      '#attributes' => [
        'placeholder' => "domain.com\nwww.domain.com",
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['#theme'] = 'system_config_form';
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->saveValues($form, $form_state);
  }

  /**
   * Save form submitted values.
   */
  public function saveValues(array &$form, FormStateInterface $form_state) {
    $param = $form_state->getValue('param');
    $allowed_domains = $form_state->getValue('allowed_domains');

    $this->config('external_register_redirect.settings')
      ->set('param', $param)
      ->set('allowed_domains', $allowed_domains)
      ->save();
  }

}
