CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module lets you configure external redirects after user registration.

 * For a full description of the module visit:
   https://www.drupal.org/project/external_register_redirect

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/external_register_redirect


REQUIREMENTS
------------

This module requires the following modules.

* `user` core module needs to be enabled.


INSTALLATION
------------

Install the External Register Redirect module as you would normally install a 
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.



CONFIGURATION
-------------

1. Navigate to `Administration > Extend` and enable the module.
2. Navigate to `Administration > Configuration > People > External Register
Redirect` and fill up the allowed domains.
3. Then click on Save configuration.



MAINTAINERS
-----------

 * Fran Garcia-Linares (fjgarlin) - https://www.drupal.org/u/fjgarlin
